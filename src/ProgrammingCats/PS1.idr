import Basic.Category
%default total
-- 1
  -- a
  f : Int -> Int
  f x = x * x

  g : Int -> Int
  g x = x + 1

  -- b
  h : Int -> Int
  h x = f (g x)
  -- h 2 = 9

  -- c
  i : Int -> Int
  i x = g (f x)
  -- i 2 = 5

-- 2
  -- a
  data ArrowObj = One | Two
  data ArrowMor : ArrowObj -> ArrowObj -> Type where
    ID_One : ArrowMor One One
    ID_Two : ArrowMor Two Two
    F      : ArrowMor One Two

  arrowIdentity : (a : ArrowObj) -> ArrowMor a a
  arrowIdentity One = ID_One
  arrowIdentity Two = ID_Two

  arrowCompose : (a, b, c : ArrowObj) ->
                 (f : ArrowMor a b) ->
                 (g : ArrowMor b c) -> ArrowMor a c
  arrowCompose One One c ID_One g = g
  arrowCompose Two Two c ID_Two g = g
  arrowCompose One Two Two F ID_Two = F

  arrowLeftIdentity : (a, b : ArrowObj) ->
                      (f : ArrowMor a b) ->
                      arrowCompose a a b (arrowIdentity a) f = f
  arrowLeftIdentity One One ID_One = Refl
  arrowLeftIdentity Two Two ID_Two = Refl
  arrowLeftIdentity One Two F = Refl

  arrowRightIdentity : (a, b : ArrowObj)
                     -> (f : ArrowMor a b)
                     -> arrowCompose a b b f (arrowIdentity b) = f
  arrowRightIdentity One One ID_One = Refl
  arrowRightIdentity Two Two ID_Two = Refl
  arrowRightIdentity One Two F = Refl

  arrowAssociativity : (a, b, c, d : ArrowObj)
                    -> (f : ArrowMor a b)
                    -> (g : ArrowMor b c)
                    -> (h : ArrowMor c d)
                    -> arrowCompose a b d f (arrowCompose b c d g h) =
                       arrowCompose a c d (arrowCompose a b c f g) h
  arrowAssociativity One One c d ID_One g h = Refl
  arrowAssociativity Two Two c d ID_Two g h = Refl
  arrowAssociativity One Two Two d F ID_Two h = Refl

  arrowCategory : Category 
  arrowCategory = MkCategory
                    ArrowObj
                    ArrowMor
                    arrowIdentity
                    arrowCompose 
                    arrowLeftIdentity
                    arrowRightIdentity
                    arrowAssociativity

    -- objects
    -- Ob(C) = {1, 2}

    -- morphisms
    -- C(1, 1) = {id_1}
    -- C(1, 2) = {f}
    -- C(2, 1) = {}
    -- C(2, 2) = {id_2}

    -- composition rule
    -- id_1 ; id_1 = id_1
    -- id_1 ; f = f
    -- f ; id_2 = f
    -- id_2 ; id_2 = id_2

    -- identity morphisms
    -- id_1 in C(1, 1)
    -- id_2 in C(2, 2)

  -- b
    -- unit law
    -- g = id_1, id_1 ; g = g and g ; id_1 = g
    -- g = f,    id_1 ; f = f and f ; id_2 = f
    -- g = id_2, id_2 ; g = g and g ; id_2 = g

    -- associative law
    -- (id_1 ; id_1) ; id_1 = id_1 = id_1 ; (id_1 ; id_1)
    -- (id_1 ; id_1) ; f = f = id_1 ; (id_1 ; f)
    -- (id_1 ; f) ; id_2 = f = id_1 ; (f ; id_2)
    -- (f ; id_2) ; id_2 = f = f ; (id_2 ; id_2)
    -- (id_2 ; id_2) ; id_2 = id_2 = id_2 ; (id_2 ; id_2)

-- 3
  -- Yes, it is forced that f ; g = id_c, because f ; g must be a morphism from c to c, and there is no other morphism from c to c than id_c. Similar reasoning for g ; f = id_d

-- 4
  -- Single object called obj, morphisms are the natural numbers,
  --   x ; y = |x - y|, id_obj = 0
  --   ||1 - 2| - 3| = 2
  --   |1 - |2 - 3|| = 0

-- 5
  -- a
  -- Unit holds because 0 + m = m and m + 0 = m,
  -- associativity holds because (m_1 + m_2) + m_3 = m_1 + (m_2 + m_3)
  
  -- b
  -- Unit holds because [] ++ m = m and m ++ [] = m
  -- associativity holds because (m_1 ++ m_2) ++ m_3 = m_1 ++ (m_2 ++ m_3)

  -- c
  -- A monoid can be viewed as a category with a single object (call it obj)
  --   with the set of morphisms C(obj, obj) = M
  -- the function * gives us the composition rule
  -- and e in M gives us the identity morphism.
  -- The Unit and associative laws of the Moniod correspond to the
  -- unit and associative laws of the category.

-- 6
  -- a
  -- 1, because 1 * 12 = 12

  -- b
  -- x * a = b, y * b = c, so (y * x) * a = c

  -- c
  -- No, because P(0,b) := { 0, 1, 2, 3, 4, ...} which is more than
  --  one morphism from 0 -> b which violates our definition of preorder

-- 7
  -- (And True) False = (\p.(\q.(pq)p) True) False
  -- -> (\q.(True q) True) False
  -- -> (True False) True = (\x.(\y.x) False) True
  -- -> \y.False True = \y.(\x.(\y.y)) True
  -- -> \x.(\y.y) = False

  -- (Or False) True = (\p.(\q.(pp)q) False) True
  -- -> (\q.(False False)q) True
  -- -> (False False) True = (\x.(\y.y) False) True
  -- -> \y.y True
  -- -> True

-- 8
  -- Y g = \f.((\x.f(xx))(\x.f(xx))) g
  -- -> (\x.g(xx)) (\x.g(xx))
  -- -> g( (\x.g(xx)) (\x.g(xx)) )
  -- -> g( g ( (\x.g(xx)) (\x.g(xx)) ) )
  -- = g( g( g( ...((\x.g(xx))(\x.g(xx)))... ) ) )
