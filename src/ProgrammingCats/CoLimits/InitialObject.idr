module CoLimits.InitialObject

import Basic.Category
import Basic.Isomorphism

%access public export
%default total
%auto_implicits off

data InitialObject :
     {cat : Category}
  -> {carrier : obj cat}
  -> Type
where
  MkInitialObject :
       {cat : Category}
    -> {carrier : obj cat}
    -> (exists  : (b : obj cat) -> mor cat carrier b)
    -> (unique  : (b : obj cat) -> (f, g : mor cat carrier b) -> f = g)
    -> InitialObject {cat} {carrier}

cat : InitialObject -> Category
cat (MkInitialObject {cat} _ _) = cat

carrier : (initialObject : InitialObject) -> obj (cat initialObject)
carrier (MkInitialObject {carrier} _ _) = carrier

composeInitialMorphisms :
     (a, b : InitialObject)
  -> mor cat (carrier a) (carrier a)
composeInitialMorphisms cat a b =
  let
    x = carrier a
    y = carrier b
  in
    compose cat x y x (exists a y) (exists b x)
{--
initialObjectsAreIsomorphic :
     (cat : Category)
  -> (a, b : InitialObject cat)
  -> Isomorphic cat (carrier a) (carrier b)
initialObjectsAreIsomorphic cat a b = buildIsomorphic
  (exists a (carrier b))
  (exists b (carrier a))
  (unique a (carrier a) (composeInitialMorphisms cat a b) (identity cat (carrier a)))
  (unique b (carrier b) (composeInitialMorphisms cat b a) (identity cat (carrier b)))
--}
