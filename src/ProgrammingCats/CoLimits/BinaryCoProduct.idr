import Basic.Category
import Basic.Functor
import CoLimits.CoProduct

import Dual.DualCategory
import Product.ProductCategory

%access public export
%default total

coProductCategory : (cat1, cat2 : Category) -> Category
coProductCategory cat1 cat2 = dualCategory (productCategory cat1 cat2)

BinaryCoProduct : Category -> Type
BinaryCoProduct cat = (a, b : obj cat) -> CoProduct cat a b

mapObj : 
     (cat : Category)
  -> (p : BinaryCoProduct cat)
  -> obj (coProductCategory cat cat) -> obj cat

mapMor :
     (cat : Category)
  -> (p : BinaryCoProduct cat)
  -> (a, b : obj (coProductCategory cat cat))
  -> mor (coProductCategory cat cat) a b

BinaryCoProductFunctor : BinaryCoProduct cat -> CFunctor (coProductCategory cat cat) cat
BinaryCoProductFunctor x {cat} = MkCFunctor
  (Main.mapObj cat x)
  (Main.mapMor cat x)
  ?preserveId
  ?preserveCompose
