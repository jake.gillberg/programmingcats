import Basic.Category
import Limits.TerminalObject
import Basic.Isomorphism

terminalObjectsAreIsomorphic : 
     (cat : Category)
  -> (a, b : TerminalObject cat)
  -> Isomorphic cat (carrier a) (carrier b)
