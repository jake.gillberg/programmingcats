import Basic.Category
import Dual.DualCategory
import Product.ProductCategory

%access public export
%default total

coProductCategory : (cat1, cat2 : Category) -> Category
coProductCategory cat1 cat2 = dualCategory (productCategory cat1 cat2)
