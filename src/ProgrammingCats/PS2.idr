import Basic.Category
import Basic.Functor
import Basic.NaturalTransformation
import Idris.TypesAsCategory
import Idris.TypesAsCategoryExtensional
import Limits.TerminalObject
import Basic.Isomorphism
import Limits.Product

%default total
-- 1
  -- 6,
  --   3 constant functors mapping all obj in set to 1,2,3
  --   and all morphisms to id_1,id_2,id_3
  --   2 functors mapping {} to 1 and all other obj to 2 or 3
  --   1 functor mapping {} to 2 and all other obj to 3

-- 2
cBoolFunc : CFunctor Idris.TypesAsCategory.typesAsCategory
                     Idris.TypesAsCategory.typesAsCategory
cBoolFunc = MkCFunctor
              (\x => Bool)
              (\a, b, x, bool => bool)
              (\a => Refl)
              (\a, b, c, f, g => Refl)

-- 3
Idris : Category
Idris = TypesAsCategoryExtensional.typesAsCategoryExtensional

diagF : CFunctor Idris Idris
diagF = Functor.idFunctor Idris

mapObj : obj Idris -> obj Idris
mapObj x = Pair x x

double : (a -> b) -> Pair a a -> Pair b b
double f (x, y) = (f x, f y)

mapMor : (a, b : obj Idris)
      -> mor Idris a b
      -> mor Idris (mapObj a) (mapObj b)
mapMor a b (MkExtensionalTypeMorphism func) =
  MkExtensionalTypeMorphism (double func)

preserveId : (a : obj Idris)
          -> Main.mapMor a a (identity Idris a) = identity Idris (Main.mapObj a)
preserveId a = funExt (\x => (case x of
                                   (a, b) => Refl))

preserveCompose :
     (a, b, c : obj Idris)
  -> (f : mor Idris a b)
  -> (g : mor Idris b c)
  -> Main.mapMor a c (compose Idris a b c f g)
     = compose Idris (Main.mapObj a) (Main.mapObj b) (Main.mapObj c)
         (Main.mapMor a b f) (Main.mapMor b c g)
preserveCompose a b c
                (MkExtensionalTypeMorphism f)
                (MkExtensionalTypeMorphism g) =
  funExt (\x => (case x of
                      (a, b) => Refl))

diagG : CFunctor Idris Idris
diagG = MkCFunctor Main.mapObj Main.mapMor Main.preserveId Main.preserveCompose

component :
     (a : obj Idris)
  -> mor Idris (mapObj Main.diagF a) (mapObj Main.diagG a)
component a = MkExtensionalTypeMorphism (\x => (x, x))

diag : NaturalTransformation Idris Idris Main.diagF Main.diagG
diag = MkNaturalTransformation Main.component
         (\a, b, f => (case f of
                            (MkExtensionalTypeMorphism func) => Refl))

-- 4 a
{--
dualPreservesIsomorphic :
     Isomorphic cat a b
  -> Isomorphic (dualCategory cat) a b
dualPreservesIsomorphic (MkIsomorphic morphism (MkIsomorphism inverse (MkInverseMorphisms lawLeft lawRight))) = MkIsomorphic inverse (buildIsomorphism inverse morphism lawLeft lawRight)

doubleDualIsCat : {cat : Category} -> dualCategory (dualCategory cat) = cat

tmp : Isomorphic (dualCategory (dualCategory cat)) a b = Isomorphic cat a b

terminalObjectsAreIsomorphic :
     (cat : Category)
  -> (a, b : TerminalObject cat)
  -> Isomorphic cat (carrier a) (carrier b)
--terminalObjectsAreIsomorphic cat a b = tmp (dualPreservesIsomorphic (initialObjectsAreIsomorphic (dualCategory cat) a b))
--}

composeTerminalMorphisms :
     (cat : Category)
  -> (a, b : TerminalObject cat)
  -> mor cat (carrier a) (carrier a)
composeTerminalMorphisms cat a b =
  let
    x = carrier a
    y = carrier b
  in
    compose cat x y x (exists b x) (exists a y)

terminalObjectsAreIsomorphic :
     (cat : Category)
  -> (a, b : TerminalObject cat)
  -> Isomorphic cat (carrier a) (carrier b)
terminalObjectsAreIsomorphic cat a b = buildIsomorphic
  (exists b (carrier a))
  (exists a (carrier b))
  (unique a (carrier a) (composeTerminalMorphisms cat a b) (identity cat (carrier a)))
  (unique b (carrier b) (composeTerminalMorphisms cat b a) (identity cat (carrier b)))

morphism :
     (cat : Category)
  -> (a, b : Product cat x y)
  -> mor cat (carrier a) (carrier b)
morphism cat a b = challenger (exists b (carrier a) (inl a) (inr a))

inverse : 
     (cat : Category)
  -> (a, b : Product cat x y)
  -> mor cat (carrier b) (carrier a)
inverse cat a b = challenger (exists a (carrier b) (inl b) (inr b))

idA : 
     (cat : Category)
  -> (a, b : Product cat x y)
  -> CommutingMorphism (dualCategory cat) x y (carrier a) (carrier a) (inl a) (inr a) (inl a) (inr a)
idA cat a b {x} {y} = MkCommutingMorphism (identity cat (carrier a)) (leftIdentity cat (carrier a) x (inl a)) (leftIdentity cat (carrier a) y (inr a))

leftInverse :
     (cat : Category)
  -> (a, b : Product cat x y)
  -> LeftInverse (morphism cat a b) (inverse cat a b)

{--
ab : compose cat
             (carrier a)
             (carrier b)
             (carrier a)
             (challenger (exists b (carrier a) (inl a) (inr a)))
             (challenger (exists a (carrier b) (inl b) (inr b))) =
     identity cat (carrier a)
--}

productsAreIsomorphic :
     (cat : Category)
  -> (a, b : Product cat x y)
  -> Isomorphic cat (carrier a) (carrier b)
productsAreIsomorphic cat a b = buildIsomorphic
  (Main.morphism cat a b)
  (Main.inverse cat a b)
  (leftInverse cat a b)
  ?rightInverse
