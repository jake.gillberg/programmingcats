import Basic.Category
import public Dual.DualCategory

import CoLimits.InitialObject
import CoLimits.CoProduct

%access public export
%default total

BinaryCoProduct : Category -> Type
BinaryCoProduct cat = (a, b : obj cat) -> CoProduct cat a b

record FiniteCoProduct (cat : Category) where
  constructor MkFiniteCoProduct
  binaryProduct : BinaryCoProduct cat
  initialObject : InitialObject cat

FiniteProduct : Category -> Type
FiniteProduct cat = FiniteCoProduct (dualCategory cat)
