import Basic.Category
import Basic.Functor
import Product.ProductCategory
import CoLimits.CoProduct
import Limits.Product
import public Dual.DualCategory

%access public export
%default total

idCommutingMorphism :
     (cat : Category)
  -> (l, r : obj cat)
  -> (a : CoProduct cat l r)
  -> CommutingMorphism
       cat
       l r (carrier a) (carrier a)
       (inl a) (inr a)
       (inl a) (inr a)
idCommutingMorphism cat l r a = MkCommutingMorphism
  (identity cat (carrier a))
  (rightIdentity cat l (carrier a) (inl a))
  (rightIdentity cat r (carrier a) (inr a))


BinaryCoProduct : Category -> Type
BinaryCoProduct cat = (a, b : obj cat) -> CoProduct cat a b

BinaryProduct : Category -> Type
BinaryProduct cat = BinaryCoProduct (dualCategory cat)

mapObj :
     (cat : Category)
  -> (p : BinaryProduct cat)
  -> obj (productCategory cat cat) -> obj cat
mapObj cat p (a, b) = carrier (p a b)

mapMor :
     (cat : Category)
  -> (p : BinaryProduct cat)
  -> (a, b : obj (productCategory cat cat))
  -> mor (productCategory cat cat) a b
  -> mor cat (Main.mapObj cat p a) (Main.mapObj cat p b)
mapMor cat p (al, ar) (bl, br) (MkProductMorphism pi1 pi2) =
  challenger (exists (p bl br)
                     (carrier (p al ar))
                     (compose cat (carrier (p al ar)) al bl (inl (p al ar)) pi1)
                     (compose cat (carrier (p al ar)) ar br (inr (p al ar)) pi2))

preserveId :
     (cat : Category)
  -> (p : BinaryProduct cat)
  -> (a : obj (productCategory cat cat))
  -> Main.mapMor cat p a a (identity (productCategory cat cat) a) = identity cat (Main.mapObj cat p a)
preserveId cat p (l, r) =
  rewrite unique (p l r)
            (carrier (p l r))
            (inl (p l r))
            (inr (p l r))
            (idCommutingMorphism (dualCategory cat) l r (p l r)) in
  rewrite rightIdentity cat (carrier (p l r)) l (inl (p l r)) in
  rewrite rightIdentity cat (carrier (p l r)) r (inr (p l r)) in Refl

preserveCompose :
     (cat : Category)
  -> (p : BinaryProduct cat)
  -> (a, b, c : obj (productCategory cat cat))
  -> (f : mor (productCategory cat cat) a b)
  -> (g : mor (productCategory cat cat) b c)
  -> Main.mapMor cat p a c (compose (productCategory cat cat) a b c f g)
    = compose cat (Main.mapObj cat p a) (Main.mapObj cat p b) (Main.mapObj cat p c)
              (Main.mapMor cat p a b f) (Main.mapMor cat p b c g)
preserveCompose cat p a b c f g = ?preserveCompose_rhs

binaryProductToFunctor : BinaryProduct cat -> CFunctor (productCategory cat cat) cat
binaryProductToFunctor x {cat} = MkCFunctor
  (Main.mapObj cat x)
  (Main.mapMor cat x)
  (Main.preserveId cat x)
  (Main.preserveCompose cat x)
