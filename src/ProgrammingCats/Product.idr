import Basic.Category
import Basic.Functor
import Basic.Isomorphism
import Cats.CatsAsCategory

%access public export
%default total
%auto_implicits off
record CommutingMorphism
 (cat : Category)
 (a : obj cat) (b : obj cat) (carrier : obj cat) (c : obj cat)
 (inl : mor cat a carrier) (inr : mor cat b carrier)
 (f : mor cat a c) (g : mor cat b c)
where
  constructor MkCommutingMorphism
  challenger         : mor cat carrier c
  commutativityLeft  : compose cat a carrier c inl challenger = f
  commutativityRight : compose cat b carrier c inr challenger = g

record CoProduct
	(cat : Category)
	(a : obj cat) (b : obj cat)
where
	constructor MkCoProduct
	carrier: obj cat
	inl: mor cat a carrier
	inr: mor cat b carrier
	exists:
			 (c : obj cat)
		-> (f : mor cat a c)
		-> (g : mor cat b c)
		-> CommutingMorphism cat a b carrier c inl inr f g
	unique:
			 (c : obj cat)
		-> (f : mor cat a c)
		-> (g : mor cat b c)
		-> (h : CommutingMorphism cat a b carrier c inl inr f g)
		-> challenger h = challenger (exists c f g)

coProductMorphism :
     (cat : Category)
  -> (l, r : obj cat)
  -> (a, b : CoProduct cat l r)
  -> CommutingMorphism
       cat
       l r (carrier a) (carrier b)
       (inl a) (inr a)
       (inl b) (inr b)
coProductMorphism cat l r a b = exists a (carrier b) (inl b) (inr b)

composeCoProductMorphisms :
     (cat : Category)
  -> (l, r : obj cat)
  -> (a, b : CoProduct cat l r)
  -> CommutingMorphism
       cat
       l r (carrier a) (carrier a)
       (inl a) (inr a)
       (inl a) (inr a)
composeCoProductMorphisms cat l r a b =
  let
    mor = coProductMorphism cat l r a b
    inv = coProductMorphism cat l r b a
  in
    MkCommutingMorphism
      (compose cat (carrier a) (carrier b) (carrier a)
        (challenger mor) (challenger inv))
      (rewrite associativity cat l (carrier a) (carrier b) (carrier a)
               (inl a) (challenger mor) (challenger inv) in
       rewrite commutativityLeft mor in
       rewrite commutativityLeft inv in Refl)
      (rewrite associativity cat r (carrier a) (carrier b) (carrier a)
               (inr a) (challenger mor) (challenger inv) in
       rewrite commutativityRight mor in
       rewrite commutativityRight inv in Refl)

idCommutingMorphism :
     (cat : Category)
  -> (l, r : obj cat)
  -> (a : CoProduct cat l r)
  -> CommutingMorphism
       cat
       l r (carrier a) (carrier a)
       (inl a) (inr a)
       (inl a) (inr a)
idCommutingMorphism cat l r a = MkCommutingMorphism
  (identity cat (carrier a))
  (rightIdentity cat l (carrier a) (inl a))
  (rightIdentity cat r (carrier a) (inr a))

coProductsAreIsomorphic :
     (cat : Category)
  -> (l, r : obj cat)
  -> (a, b : CoProduct cat l r)
  -> Isomorphic cat (carrier a) (carrier b)
coProductsAreIsomorphic cat l r a b =
  let
    mor = coProductMorphism cat l r a b
    inv = coProductMorphism cat l r b a
  in
    buildIsomorphic
      (challenger mor)
      (challenger inv)
      (rewrite unique a (carrier a) (inl a) (inr a)
                        (composeCoProductMorphisms cat l r a b) in
       rewrite unique a (carrier a) (inl a) (inr a)
                        (idCommutingMorphism cat l r a) in Refl)
      (rewrite unique b (carrier b) (inl b) (inr b)
                        (composeCoProductMorphisms cat l r b a) in
       rewrite unique b (carrier b) (inl b) (inr b)
                        (idCommutingMorphism cat l r b) in Refl)
dualMorphism :
     (cat : Category)
  -> (a, b  : obj cat)
  -> Type
dualMorphism cat a b = mor cat b a

dualCompose :
     (cat : Category)
  -> (a, b, c : obj cat)
  -> (f : dualMorphism cat a b)
  -> (g : dualMorphism cat b c)
  -> dualMorphism cat a c
dualCompose cat a b c f g = (compose cat) c b a g f

dualAssoc :
     (cat : Category)
  -> (a, b, c, d : obj cat)
  -> (f : dualMorphism cat a b)
  -> (g : dualMorphism cat b c)
  -> (h : dualMorphism cat c d)
  ->  dualCompose cat a b d f (dualCompose cat b c d g h)
      = dualCompose cat a c d (dualCompose cat a b c f g) h
dualAssoc cat a b c d f g h = sym (associativity cat d c b a h g f)

dualLeftIdentity :
     (cat : Category)
  -> (a, b : obj cat)
  -> (f : dualMorphism cat a b)
  -> dualCompose cat a a b (identity cat a) f = f
dualLeftIdentity cat a b f = rightIdentity cat b a f

dualRightIdentity :
     (cat : Category)
  -> (a, b : obj cat)
  -> (f : dualMorphism cat a b)
  -> dualCompose cat a b b f (identity cat b) = f
dualRightIdentity cat a b f = leftIdentity cat b a f

dualCategory : (cat : Category) -> Category
dualCategory cat = MkCategory
  (obj cat)
  (dualMorphism cat)
  (identity cat)
  (dualCompose cat)
  (dualLeftIdentity cat)
  (dualRightIdentity cat)
  (dualAssoc cat)

doubleDualTo : (cat : Category) -> CFunctor (dualCategory $ dualCategory cat) cat
doubleDualTo cat = MkCFunctor
  id
  (\_, _ => id)
  (\_ => Refl)
  (\_, _, _, _, _ => Refl)

doubleDualFrom : (cat : Category) -> CFunctor cat (dualCategory $ dualCategory cat)
doubleDualFrom cat = MkCFunctor
  id
  (\_, _ => id)
  (\_ => Refl)
  (\_, _, _, _, _ => Refl)


doubleDualIsomorphism :
     (cat : Category)
  -> Isomorphic CatsAsCategory.catsAsCategory
                 cat
                 (dualCategory $ dualCategory cat)
doubleDualIsomorphism cat = buildIsomorphic
  (doubleDualFrom cat)
  (doubleDualTo   cat)
  (functorEq cat
             cat
             (functorComposition cat
                                 (dualCategory $ dualCategory cat)
                                 cat
                                 (doubleDualFrom cat)
                                 (doubleDualTo cat))
             (idFunctor cat)
             (\_ => Refl)
             (\_, _, _ => Refl))
  (functorEq (dualCategory $ dualCategory cat)
             (dualCategory $ dualCategory cat)
             (functorComposition (dualCategory $ dualCategory cat)
                                 cat
                                 (dualCategory $ dualCategory cat)
                                 (doubleDualTo cat)
                                 (doubleDualFrom cat))
             (idFunctor (dualCategory $ dualCategory cat))
             (\_ => Refl)
             (\_, _, _ => Refl))

dualPreservesIsomorphic :
     (cat : Category)
  -> (a, b : obj cat)
  -> Isomorphic (dualCategory cat) a b
  -> Isomorphic cat a b
dualPreservesIsomorphic cat a b
  (MkIsomorphic
    morphism
    (MkIsomorphism inverse
      (MkInverseMorphisms lawLeft lawRight))) =
  MkIsomorphic
    inverse
    (MkIsomorphism
      morphism
      (MkInverseMorphisms lawLeft lawRight))
Product : (cat : Category) -> (l : obj cat) -> (r : obj cat) -> Type
Product cat l r = CoProduct (dualCategory cat) l r

productsAreIsomorphic :
     (cat : Category)
  -> (l, r : obj cat)
  -> (a, b : Product cat l r)
  -> Isomorphic cat (carrier a) (carrier b)
productsAreIsomorphic cat l r a b = dualPreservesIsomorphic cat (carrier a) (carrier b) (coProductsAreIsomorphic (dualCategory cat) l r a b)
